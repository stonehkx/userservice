package com.csdn.userservice.model;

import com.baomidou.mybatisplus.annotation.TableField;

import java.util.ArrayList;
import java.util.List;

/**
 * @author StoneHkx
 * @ClassName SUser
 * @Description TODO
 * @createDate 2020-01-05 11:08
 * @updatePerson
 * @updateDate
 */
public class SUser extends BaseModel {
    private String userName;
    private String password;

    @TableField(exist = false)
    private List<SRole> roles;
    @TableField(exist = false)
    private List<SPermission> permissions;

    @TableField(exist = false)
    private List<Long> roleIds;

    @TableField(exist = false)
    private List<String> buttons = new ArrayList<>();


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<SRole> getRoles() {
        return roles;
    }

    public void setRoles(List<SRole> roles) {
        this.roles = roles;
    }

    public List<SPermission> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<SPermission> permissions) {
        this.permissions = permissions;
    }

    public List<Long> getRoleIds() {
        return roleIds;
    }

    public void setRoleIds(List<Long> roleIds) {
        this.roleIds = roleIds;
    }

    public List<String> getButtons() {
        return buttons;
    }

    public void setButtons(List<String> buttons) {
        this.buttons = buttons;
    }
}
