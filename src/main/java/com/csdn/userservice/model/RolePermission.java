package com.csdn.userservice.model;

/**
 * @author StoneHkx
 * @ClassName SRole
 * @Description TODO
 * @createDate 2020-01-05 11:12
 * @updatePerson
 * @updateDate
 */
public class RolePermission extends BaseModel {
    private Long roleId;
    private Long permissionId;

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public Long getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(Long permissionId) {
        this.permissionId = permissionId;
    }
}
