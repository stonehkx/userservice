package com.csdn.userservice.model;

/**
 * @author StoneHkx
 * @ClassName SRole
 * @Description TODO
 * @createDate 2020-01-05 11:12
 * @updatePerson
 * @updateDate
 */
public class UserRole extends BaseModel {
    private int userId;
    private int roleId;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }
}
