package com.csdn.userservice.model;

/**
 * @author StoneHkx
 * @ClassName SPermission
 * @Description TODO
 * @createDate 2020-01-05 11:14
 * @updatePerson
 * @updateDate
 */
public class SPermission extends BaseModel {
    private String permissionName;
    private int parentId;
    private String url;
    private boolean button;
    private String icon;
    private String tright;
    private String orderDesc;

    public String getPermissionName() {
        return permissionName;
    }

    public void setPermissionName(String permissionName) {
        this.permissionName = permissionName;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean getButton() {
        return button;
    }

    public void setButton(boolean button) {
        this.button = button;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getTright() {
        return tright;
    }

    public void setTright(String tright) {
        this.tright = tright;
    }

    public String getOrderDesc() {
        return orderDesc;
    }

    public void setOrderDesc(String orderDesc) {
        this.orderDesc = orderDesc;
    }
}
