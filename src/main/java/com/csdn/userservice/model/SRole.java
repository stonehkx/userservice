package com.csdn.userservice.model;

import com.baomidou.mybatisplus.annotation.TableField;

import java.util.List;

/**
 * @author StoneHkx
 * @ClassName SRole
 * @Description TODO
 * @createDate 2020-01-05 11:12
 * @updatePerson
 * @updateDate
 */
public class SRole extends BaseModel {

    private String roleName;
    private Long createById;

    @TableField(exist = false)
    private List<SPermission> permission;

    @TableField(exist = false)
    private List<Long> permissionIds;

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public List<SPermission> getPermission() {
        return permission;
    }

    public void setPermission(List<SPermission> permission) {
        this.permission = permission;
    }

    public List<Long> getPermissionIds() {
        return permissionIds;
    }

    public void setPermissionIds(List<Long> permissionIds) {
        this.permissionIds = permissionIds;
    }

    public Long getCreateById() {
        return createById;
    }

    public void setCreateById(Long createById) {
        this.createById = createById;
    }
}
