package com.csdn.userservice.feign.hystrix;

import com.csdn.userservice.feign.Maindatafeign;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * @author StoneHkx
 * @ClassName Maindataservice
 * @Description TODO
 * @createDate 2020-01-04 00:58
 * @updatePerson
 * @updateDate
 */
@Component
public class Maindataservice implements Maindatafeign {

    @Override
    public Map<String, Object> getProduct() {
        Map<String,Object> map = new HashMap<>();
        map.put("result",false);
        map.put("data","调用失败，服务降级hystrix");
        return map;
    }
}
