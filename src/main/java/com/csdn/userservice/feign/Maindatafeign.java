package com.csdn.userservice.feign;

/**
 * @author StoneHkx
 * @ClassName Maindatafeign
 * @Description TODO
 * @createDate 2020-01-04 00:54
 * @updatePerson
 * @updateDate
 */

import com.csdn.userservice.feign.hystrix.Maindataservice;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.Map;

@FeignClient(value = "maindataservice",fallback = Maindataservice.class)
public interface Maindatafeign {

    @PostMapping("getProduct")
    Map<String,Object> getProduct();

}
