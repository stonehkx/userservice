package com.csdn.userservice.controller;

import com.csdn.userservice.feign.Maindatafeign;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.ribbon.proxy.annotation.Hystrix;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.http.*;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

/**
 * @author StoneHkx
 * @ClassName Test
 * @Description TODO
 * @createDate 2019-12-30 23:50
 * @updatePerson
 * @updateDate
 */
@EnableHystrix
@RestController
public class Test {

    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private Maindatafeign maindatafeign;


    @HystrixCommand(fallbackMethod = "hystrixMethod")
    @RequestMapping("/test")
    public String getRibon(){
        Map pMap = new HashMap();
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpEntity<Object> entity = new HttpEntity<>(pMap, httpHeaders);
        String data = "";
        try {
            ResponseEntity<Map> map = restTemplate.exchange("http://maindataservice/getProduct", HttpMethod.POST, entity, Map.class);
            if (map.getStatusCode() == HttpStatus.OK) {
                Map bodyMap = map.getBody();
                if(!(Boolean)bodyMap.get("result")){
                    return "调用异常1";
                }
            }else{
               return "调用异常2";
            }
            data = (String)map.getBody().get("data");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public String hystrixMethod(){
        return "调用失败，已经调用了hystrix回调方法！";
    }

    @RequestMapping("/feign")
    public String feign(){
        Map<String, Object> product = maindatafeign.getProduct();
        if((Boolean) product.get("result")){
            return (String)product.get("data");
        }
        return "result为false";
    }

}
