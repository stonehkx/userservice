package com.csdn.userservice.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.csdn.userservice.model.SPermission;
import com.csdn.userservice.model.SRole;
import com.csdn.userservice.service.SPermissionService;
import com.csdn.userservice.service.SRoleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @author StoneHkx
 * @ClassName SRoleContrller
 * @Description TODO
 * @createDate 2020-01-05 15:21
 * @updatePerson
 * @updateDate
 */
@RestController
@RequestMapping("/permission")
public class SPermissionContrller {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    @Qualifier("SPermissionServicelmpl")
    private SPermissionService permissionService;

    /*
     * @Description: 保存角色接口
     * @Param: [user]
     * @return: java.util.Map<java.lang.String,java.lang.Object>
     * @Author: StoneHkx
     * @Date: 2020/1/5 11:56
     */
    @PostMapping("/savePermission")
    public Map<String,Object> savePermission(@RequestBody SPermission permission){
        Map<String,Object> result = new HashMap<>();
        try {
            SPermission data= permissionService.savePermission(permission);
            result.put("保存权限成功",data);
            return result;
        }catch (Exception e){
            log.error("保存权限异常"+e.getMessage());
            result.put("保存权限异常",e.getMessage());
            return result;
        }
    }

    /*
     * @Description: 更新用户接口
     * @Param: [user]
     * @return: java.util.Map<java.lang.String,java.lang.Object>
     * @Author: StoneHkx
     * @Date: 2020/1/5 11:56
     */
    @PostMapping("/updatePermission")
    public Map<String,Object> updatePermission(@RequestBody SPermission permission){
        Map<String,Object> result = new HashMap<>();
        try {
            SPermission data= permissionService.updatePermission(permission);
            result.put("更新权限成功",data);
            return result;
        }catch (Exception e){
            log.error("更新权限异常"+e.getMessage());
            result.put("更新权限异常",e.getMessage());
            return result;
        }
    }

    /*
     * @Description: 查询角色接口
     * @Param: [user]
     * @return: java.util.Map<java.lang.String,java.lang.Object>
     * @Author: StoneHkx
     * @Date: 2020/1/5 11:56
     */
    @PostMapping("/selectPermission")
    public Map<String,Object> selectPermission(@RequestBody SPermission permission){
        Map<String,Object> result = new HashMap<>();
        try {
            IPage<SPermission> data= permissionService.selectPermission(permission);
            result.put("查询权限成功",data);
            return result;
        }catch (Exception e){
            log.error("查询权限异常"+e.getMessage());
            result.put("查询权限异常",e.getMessage());
            return result;
        }
    }
}
