package com.csdn.userservice.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.csdn.userservice.model.SUser;
import com.csdn.userservice.service.SUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @author StoneHkx
 * @ClassName SUserController
 * @Description TODO
 * @createDate 2020-01-05 11:43
 * @updatePerson
 * @updateDate
 */
@RestController
@RequestMapping("/user")
public class SUserController {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    @Qualifier("SUserServicelmpl")
    private SUserService userService;

    /*
    * @Description: 保存用户接口
    * @Param: [user]
    * @return: java.util.Map<java.lang.String,java.lang.Object>
    * @Author: StoneHkx
    * @Date: 2020/1/5 11:56
    */
    @PostMapping("/saveUser")
    public Map<String,Object> saveUser(@RequestBody SUser user){
        Map<String,Object> result = new HashMap<>();
        try {
            SUser data= userService.saveUser(user);
            result.put("保存用户成功",data);
            return result;
        }catch (Exception e){
            log.error("保存用户异常"+e.getMessage());
            result.put("保存用户异常",e.getMessage());
            return result;
        }
    }

    /*
     * @Description: 更新用户接口
     * @Param: [user]
     * @return: java.util.Map<java.lang.String,java.lang.Object>
     * @Author: StoneHkx
     * @Date: 2020/1/5 11:56
     */
    @PostMapping("/updateUser")
    public Map<String,Object> updateUser(@RequestBody SUser user){
        Map<String,Object> result = new HashMap<>();
        try {
            SUser data= userService.updateUser(user);
            result.put("更新用户成功",data);
            return result;
        }catch (Exception e){
            log.error("更新用户异常"+e.getMessage());
            result.put("更新用户异常",e.getMessage());
            return result;
        }
    }

    /*
     * @Description: 查询用户接口
     * @Param: [user]
     * @return: java.util.Map<java.lang.String,java.lang.Object>
     * @Author: StoneHkx
     * @Date: 2020/1/5 11:56
     */
    @PostMapping("/selectUser")
    public Map<String,Object> selectUser(@RequestBody SUser user){
        Map<String,Object> result = new HashMap<>();
        try {
            IPage<SUser> data= userService.selectUser(user);
            result.put("result",true);
            result.put("data",data);
            return result;
        }catch (Exception e){
            log.error("查询用户异常"+e.getMessage());
            result.put("result",false);
            result.put("data",e.getMessage());
            return result;
        }
    }

    /*
     * @Description: 获取用户信息
     * @Param: [user]
     * @return: java.util.Map<java.lang.String,java.lang.Object>
     * @Author: StoneHkx
     * @Date: 2020/1/5 11:56
     */
    @PostMapping("/getUserByNamePassword")
    public Map<String,Object> getUserByNamePassword(@RequestBody SUser user){
        Map<String,Object> result = new HashMap<>();
        try {
            SUser data= userService.getUserByNamePassword(user);
            result.put("result",true);
            result.put("data", JSONArray.toJSONString(data));
            return result;
        }catch (Exception e){
            log.error("获取用户异常"+e.getMessage());
            result.put("result",false);
            result.put("data",e.getMessage());
            return result;
        }
    }

}
