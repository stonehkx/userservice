package com.csdn.userservice.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.csdn.userservice.model.SRole;
import com.csdn.userservice.model.SUser;
import com.csdn.userservice.service.SRoleService;
import com.csdn.userservice.service.SUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * @author StoneHkx
 * @ClassName SRoleContrller
 * @Description TODO
 * @createDate 2020-01-05 15:21
 * @updatePerson
 * @updateDate
 */
@RestController
@RequestMapping("/role")
public class SRoleContrller {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    @Qualifier("SRoleServicelmpl")
    private SRoleService roleService;

    /*
     * @Description: 保存角色接口
     * @Param: [user]
     * @return: java.util.Map<java.lang.String,java.lang.Object>
     * @Author: StoneHkx
     * @Date: 2020/1/5 11:56
     */
    @PostMapping("/saveRole")
    public Map<String,Object> saveRole(HttpServletRequest request, @RequestBody SRole role){
        Map<String,Object> result = new HashMap<>();
        try {
            String token = request.getHeader("TOKEN");
            SRole data= roleService.saveRole(role,token);
            result.put("保存角色成功",data);
            return result;
        }catch (Exception e){
            log.error("保存角色异常"+e.getMessage());
            result.put("保存角色异常",e.getMessage());
            return result;
        }
    }

    /*
     * @Description: 更新用户接口
     * @Param: [user]
     * @return: java.util.Map<java.lang.String,java.lang.Object>
     * @Author: StoneHkx
     * @Date: 2020/1/5 11:56
     */
    @PostMapping("/updateRole")
    public Map<String,Object> updateRole(@RequestBody SRole role){
        Map<String,Object> result = new HashMap<>();
        try {
            SRole data= roleService.updateRole(role);
            result.put("更新角色成功",data);
            return result;
        }catch (Exception e){
            log.error("更新角色异常"+e.getMessage());
            result.put("更新角色异常",e.getMessage());
            return result;
        }
    }

    /*
     * @Description: 查询角色接口
     * @Param: [user]
     * @return: java.util.Map<java.lang.String,java.lang.Object>
     * @Author: StoneHkx
     * @Date: 2020/1/5 11:56
     */
    @PostMapping("/selectRole")
    public Map<String,Object> selectRole(HttpServletRequest request,@RequestBody(required = false) SRole role){
        Map<String,Object> result = new HashMap<>();
        try {
            String token = request.getHeader("TOKEN");
            if(StringUtils.isEmpty(role)){
                role = new SRole();
            }
            IPage<SRole> data= roleService.selectRole(role,token);
            result.put("查询角色成功",data);
            return result;
        }catch (Exception e){
            log.error("查询角色异常"+e.getMessage());
            result.put("查询角色异常",e.getMessage());
            return result;
        }
    }
}
