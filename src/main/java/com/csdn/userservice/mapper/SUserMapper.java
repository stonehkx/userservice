package com.csdn.userservice.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.csdn.userservice.model.SUser;

/**
 * @author StoneHkx
 * @ClassName SUserMapper
 * @Description TODO
 * @createDate 2020-01-05 11:52
 * @updatePerson
 * @updateDate
 */
public interface SUserMapper extends BaseMapper<SUser> {

}
