package com.csdn.userservice.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.csdn.userservice.model.SPermission;
import com.csdn.userservice.model.SRole;

/**
 * @author StoneHkx
 * @ClassName SRoleMapper
 * @Description TODO
 * @createDate 2020-01-05 15:38
 * @updatePerson
 * @updateDate
 */
public interface SPermissionMapper extends BaseMapper<SPermission> {
}
