package com.csdn.userservice.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.csdn.userservice.model.SUser;
import com.csdn.userservice.model.UserRole;

/**
 * @author StoneHkx
 * @ClassName SUserMapper
 * @Description TODO
 * @createDate 2020-01-05 11:52
 * @updatePerson
 * @updateDate
 */
public interface SUserRoleMapper extends BaseMapper<UserRole> {

}
