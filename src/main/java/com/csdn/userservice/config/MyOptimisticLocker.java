package com.csdn.userservice.config;

import com.baomidou.mybatisplus.extension.plugins.OptimisticLockerInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author StoneHkx
 * @ClassName MyOptimisticLocker
 * @Description TODO
 * @createDate 2020-01-05 13:03
 * @updatePerson
 * @updateDate
 */
@Configuration
public class MyOptimisticLocker {
    @Bean
    public OptimisticLockerInterceptor optimisticLockerInterceptor() {
        return new OptimisticLockerInterceptor();
    }
}
