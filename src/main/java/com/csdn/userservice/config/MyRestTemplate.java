package com.csdn.userservice.config;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @author StoneHkx
 * @ClassName MyRestTemplate
 * @Description TODO
 * @createDate 2020-01-04 00:51
 * @updatePerson
 * @updateDate
 */
@Configuration
public class MyRestTemplate {
    @Bean
    @LoadBalanced
    public RestTemplate getRestTemplate(){
        return new RestTemplate();
    }
}
