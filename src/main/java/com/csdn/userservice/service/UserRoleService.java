package com.csdn.userservice.service;

import com.baomidou.mybatisplus.extension.service.IService;

import com.csdn.userservice.model.UserRole;

import java.util.List;

/**
 * @author StoneHkx
 * @ClassName UserRoleService
 * @Description TODO
 * @createDate 2020-01-05 17:41
 * @updatePerson
 * @updateDate
 */
public interface UserRoleService extends IService<UserRole> {
    boolean insertBatch(List<UserRole> list);
}
