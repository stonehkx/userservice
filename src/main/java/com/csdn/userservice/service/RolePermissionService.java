package com.csdn.userservice.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.csdn.userservice.model.RolePermission;
import com.csdn.userservice.model.SPermission;

import java.util.List;

/**
 * @author StoneHkx
 * @ClassName RolePermissionService
 * @Description TODO
 * @createDate 2020-01-05 17:35
 * @updatePerson
 * @updateDate
 */
public interface RolePermissionService  extends IService<RolePermission> {

    boolean insertBatch(List<RolePermission> list);
}
