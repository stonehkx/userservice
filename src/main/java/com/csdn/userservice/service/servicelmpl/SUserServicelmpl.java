package com.csdn.userservice.service.servicelmpl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.csdn.userservice.mapper.*;
import com.csdn.userservice.model.*;
import com.csdn.userservice.service.SRoleService;
import com.csdn.userservice.service.SUserService;
import com.csdn.userservice.service.UserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author StoneHkx
 * @ClassName SUserServicelmpl
 * @Description TODO
 * @createDate 2020-01-05 11:51
 * @updatePerson
 * @updateDate
 */
@Service("SUserServicelmpl")
public class SUserServicelmpl extends ServiceImpl<SUserMapper, SUser> implements SUserService {

    @Autowired
    @Qualifier("userRoleServicelmpl")
    private UserRoleService userRoleService;
    @Resource
    private SUserRoleMapper userRoleMapper;
    @Resource
    private SRoleMapper roleMapper;
    @Resource
    private SRolePermissionMapper rolePermissionMapper;
    @Resource
    private SPermissionMapper permissionMapper;

    @Autowired
    private SRoleService roleService;


    @Override
    @Transactional
    public SUser saveUser(SUser user) throws Exception{

        if(this.save(user)){
            return user;
        }else{
            throw new Exception("保存用户失败");
        }
    }

    @Override
    @Transactional
    public SUser updateUser(SUser user) throws Exception {
        List<UserRole> newRoles = new ArrayList<>();
        List<UserRole> oldRoles = new ArrayList<>();
        List<UserRole> updateUserRoles = new ArrayList<>();
        //更新角色的权限
        if(!CollectionUtils.isEmpty(user.getRoleIds())){
            newRoles = this.userRoleMapper.selectBatchIds(user.getRoleIds());
            oldRoles = this.userRoleMapper.selectList(new LambdaQueryWrapper<UserRole>().eq(UserRole::getUserId,user));
        }

        if(!CollectionUtils.isEmpty(newRoles)){
            boolean exist;
            //老的没有直接增加
            if(CollectionUtils.isEmpty(oldRoles)){
                this.userRoleService.insertBatch(newRoles);
            }else{
                //新的没有老的有删除
                for(UserRole newRole : newRoles){
                    exist = false;
                    for(UserRole oldRole : oldRoles){
                        if(oldRole.getId().equals(newRole.getId())){
                            exist = true;
                        }
                    }
                    if(exist){
                        UserRole existRole = new UserRole();
                        existRole.setId(newRole.getId());
                        existRole.setIsDelete(true);
                        updateUserRoles.add(existRole);
                    }
                }
                //新的有老的没有增加
                for(UserRole oldRole : oldRoles){
                    for(UserRole newRole : newRoles){
                        if(oldRole.getId().equals(newRole.getId())){
                            updateUserRoles.add(newRole);
                        }
                    }
                }
            }
        }

        //更新角色
        this.userRoleService.insertBatch(updateUserRoles);

        //更新用户
        this.baseMapper.updateById(user);
        return user;
    }

    @Override
    public IPage<SUser> selectUser(SUser user) {
        Page<SUser> page = new Page<>();
        page.setCurrent(user.getCurrent());
        page.getSize();

        LambdaQueryWrapper<SUser> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.like(SUser::getUserName,user.getUserName());
        //queryWrapper.setEntity(user);

        return this.page(page,queryWrapper);
    }

    @Override
    public SUser getUserByNamePassword(SUser user) throws Exception {

        LambdaQueryWrapper<SUser> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.eq(SUser::getUserName,user.getUserName());
        queryWrapper.eq(SUser::getPassword,user.getPassword());
        queryWrapper.eq(SUser::getIsDelete,false);

        SUser loginUser = this.getOne(queryWrapper);

        if(StringUtils.isEmpty(loginUser)){
            throw new Exception("用户名或密码错误！");
        }

        List<UserRole> userRoles = this.userRoleMapper.selectList(new LambdaQueryWrapper<UserRole>().eq(UserRole::getUserId, loginUser.getId()).eq(UserRole::getIsDelete, false));

        if(!CollectionUtils.isEmpty(userRoles)){
            List<Integer> roleIds = userRoles.stream().map(UserRole::getRoleId).collect(Collectors.toList());
            if(!CollectionUtils.isEmpty(roleIds)){
                List<SRole> roleList = this.roleMapper.selectBatchIds(roleIds);
                if(!CollectionUtils.isEmpty(roleList)){
                    loginUser.setRoles(roleList);
                }

                List<RolePermission> rolePermissions = this.rolePermissionMapper.selectList(new LambdaQueryWrapper<RolePermission>().in(RolePermission::getRoleId, roleIds).eq(RolePermission::getIsDelete, false));
                if(!CollectionUtils.isEmpty(rolePermissions)){
                    List<Long> permissionIds = rolePermissions.stream().map(RolePermission::getPermissionId).collect(Collectors.toList());
                    if(!CollectionUtils.isEmpty(permissionIds)){
                        List<SPermission> permissionList = this.permissionMapper.selectBatchIds(permissionIds);
                        if(!CollectionUtils.isEmpty(permissionList)){
                            loginUser.setPermissions(permissionList);
                            permissionList.forEach(tempPermission->{
                                if(tempPermission.getButton()){
                                    loginUser.getButtons().add(tempPermission.getTright());
                                }
                            });
                        }
                    }
                }

            }
        }

        /*List<UserRole> userRoles = this.userRoleMapper.selectList(new LambdaQueryWrapper<UserRole>().eq(UserRole::getUserId,loginUser.getId()).eq(UserRole::getIsDelete, false));
        if(!CollectionUtils.isEmpty(userRoles)){
            List<Integer> roleIds = userRoles.stream().map(UserRole::getRoleId).collect(Collectors.toList());
            if(!CollectionUtils.isEmpty(roleIds)){
                List<SRole> roleList = this.roleMapper.selectBatchIds(roleIds);
                if(!CollectionUtils.isEmpty(roleList)){
                    loginUser.setRoles(roleList);
                }
                List<RolePermission> rolePermissions = this.rolePermissionMapper.selectList(new LambdaQueryWrapper<RolePermission>().eq(RolePermission::getIsDelete, false).in(RolePermission::getRoleId, roleIds));
                if(!CollectionUtils.isEmpty(rolePermissions)){
                    List<Long> permissionIds = rolePermissions.stream().map(RolePermission::getPermissionId).collect(Collectors.toList());
                    if(!CollectionUtils.isEmpty(permissionIds)){
                        List<SPermission> permissionList = this.permissionMapper.selectBatchIds(permissionIds);
                        if(!CollectionUtils.isEmpty(permissionList)){
                            loginUser.setPermissions(permissionList);
                            permissionList.forEach(tempPermission->{
                                if(tempPermission.isButton()){
                                    loginUser.getButtons().add(tempPermission.getTright());
                                }
                            });
                        }
                    }
                }
            }
        }*/
        return loginUser;
    }
}
