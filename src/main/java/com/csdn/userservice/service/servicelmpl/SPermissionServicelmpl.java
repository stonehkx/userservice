package com.csdn.userservice.service.servicelmpl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.csdn.userservice.mapper.SPermissionMapper;
import com.csdn.userservice.mapper.SRoleMapper;
import com.csdn.userservice.model.SPermission;
import com.csdn.userservice.model.SRole;
import com.csdn.userservice.model.SUser;
import com.csdn.userservice.service.SPermissionService;
import com.csdn.userservice.service.SRoleService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author StoneHkx
 * @ClassName SUserServicelmpl
 * @Description TODO
 * @createDate 2020-01-05 11:51
 * @updatePerson
 * @updateDate
 */
@Service("SPermissionServicelmpl")
public class SPermissionServicelmpl extends ServiceImpl<SPermissionMapper, SPermission> implements SPermissionService {

    @Override
    public SPermission savePermission(SPermission permission) throws Exception {
        if(this.save(permission)){
            return permission;
        }else{
            throw new Exception("保存用户失败");
        }
    }

    @Override
    public SPermission updatePermission(SPermission permission) throws Exception {
        if(this.save(permission)){
            return permission;
        }else{
            throw new Exception("更新用户失败");
        }
    }

    @Override
    public IPage<SPermission> selectPermission(SPermission permission) throws Exception {
        Page<SPermission> page = new Page<>();
        page.setCurrent(permission.getCurrent());
        page.getSize();

        LambdaQueryWrapper<SPermission> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.like(SPermission::getPermissionName,permission.getPermissionName());
        //queryWrapper.setEntity(user);

        return this.page(page,queryWrapper);
    }
}
