package com.csdn.userservice.service.servicelmpl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.csdn.userservice.mapper.SUserRoleMapper;
import com.csdn.userservice.model.UserRole;
import com.csdn.userservice.service.UserRoleService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author StoneHkx
 * @ClassName UserRoleServicelmpl
 * @Description TODO
 * @createDate 2020-01-05 17:42
 * @updatePerson
 * @updateDate
 */
@Service("userRoleServicelmpl")
public class UserRoleServicelmpl extends ServiceImpl<SUserRoleMapper, UserRole> implements UserRoleService {

    @Override
    public boolean insertBatch(List<UserRole> list) {
        return this.saveBatch(list);
    }
}
