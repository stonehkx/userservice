package com.csdn.userservice.service.servicelmpl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.csdn.userservice.mapper.SRolePermissionMapper;
import com.csdn.userservice.model.RolePermission;
import com.csdn.userservice.service.RolePermissionService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author StoneHkx
 * @ClassName RolePermissionServicelmpl
 * @Description TODO
 * @createDate 2020-01-05 17:36
 * @updatePerson
 * @updateDate
 */
@Service("rolePermissionServicelmpl")
public class RolePermissionServicelmpl extends ServiceImpl<SRolePermissionMapper, RolePermission> implements RolePermissionService {
    @Override
    public boolean insertBatch(List<RolePermission> list) {
        return this.saveBatch(list);
    }
}
