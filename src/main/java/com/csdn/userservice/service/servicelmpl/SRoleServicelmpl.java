package com.csdn.userservice.service.servicelmpl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.csdn.userservice.mapper.SPermissionMapper;
import com.csdn.userservice.mapper.SRoleMapper;
import com.csdn.userservice.mapper.SRolePermissionMapper;
import com.csdn.userservice.model.RolePermission;
import com.csdn.userservice.model.SRole;
import com.csdn.userservice.model.SUser;
import com.csdn.userservice.service.RolePermissionService;
import com.csdn.userservice.service.SRoleService;
import com.csdn.userservice.service.SUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author StoneHkx
 * @ClassName SUserServicelmpl
 * @Description TODO
 * @createDate 2020-01-05 11:51
 * @updatePerson
 * @updateDate
 */
@Service("SRoleServicelmpl")
public class SRoleServicelmpl extends ServiceImpl<SRoleMapper, SRole> implements SRoleService {

    @Autowired
    @Qualifier("rolePermissionServicelmpl")
    private RolePermissionService rolePermissionService;
    @Resource
    private SRolePermissionMapper rolePermissionMapper;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Override
    @Transactional
    public SRole saveRole(SRole role,String token) throws Exception{
        String user = stringRedisTemplate.opsForValue().get(token);
        SUser currentUser = JSON.parseObject(user, SUser.class);
        role.setCreateById(currentUser.getId());
        this.baseMapper.insert(role);

        //保存角色权限（前端给的权限id集合）
        Long roleId = role.getId();
        List<RolePermission> insertList = new ArrayList<>();
        if(!CollectionUtils.isEmpty(role.getPermission())){
            role.getPermissionIds().forEach(temp->{
                RolePermission tempRP = new RolePermission();
                tempRP.setRoleId(roleId);
                tempRP.setPermissionId(temp);
                insertList.add(tempRP);
            });
        }
        if(!CollectionUtils.isEmpty(insertList)){
            rolePermissionService.insertBatch(insertList);
        }

        return role;
    }

    @Override
    @Transactional
    public SRole updateRole(SRole role) throws Exception {
        List<RolePermission> newPermissions = new ArrayList<>();
        List<RolePermission> oldPermissions = new ArrayList<>();
        List<RolePermission> updateRolePermissions = new ArrayList<>();
        //更新角色的权限
        if(!CollectionUtils.isEmpty(role.getPermissionIds())){
            newPermissions = this.rolePermissionMapper.selectBatchIds(role.getPermissionIds());
            oldPermissions = this.rolePermissionMapper.selectList(new LambdaQueryWrapper<RolePermission>().eq(RolePermission::getRoleId,role));
        }

        if(!CollectionUtils.isEmpty(newPermissions)){
            boolean exist;
            //老的没有直接增加
            if(CollectionUtils.isEmpty(oldPermissions)){
                this.rolePermissionService.insertBatch(newPermissions);
            }else{
                //新的没有老的有删除
                for(RolePermission newPermission : newPermissions){
                    exist = false;
                    for(RolePermission oldPermission : oldPermissions){
                        if(oldPermission.getId().equals(newPermission.getId())){
                            exist = true;
                        }
                    }
                    if(exist){
                        RolePermission existPermission = new RolePermission();
                        existPermission.setId(newPermission.getId());
                        existPermission.setIsDelete(true);
                        updateRolePermissions.add(existPermission);
                    }
                }
                //新的有老的没有增加
                for(RolePermission oldPermission : oldPermissions){
                    for(RolePermission newPermission : newPermissions){
                        if(oldPermission.getId().equals(newPermission.getId())){
                            updateRolePermissions.add(newPermission);
                        }
                    }
                }
            }
        }

        //更新权限
        this.rolePermissionService.insertBatch(updateRolePermissions);

        //更新角色
        this.baseMapper.updateById(role);
        return role;
    }

    @Override
    public IPage<SRole> selectRole(SRole role,String token) throws Exception {

        String user = stringRedisTemplate.opsForValue().get(token);
        SUser currentUser = JSON.parseObject(user, SUser.class);

        Page<SRole> page = new Page<>();
        /*page.setCurrent(role.getCurrent());
        page.getSize();*/

        LambdaQueryWrapper<SRole> queryWrapper = Wrappers.lambdaQuery();
        if(!StringUtils.isEmpty(role.getRoleName())){
            queryWrapper.like(SRole::getRoleName,role.getRoleName());
        }
        queryWrapper.eq(SRole::getIsDelete,false);
        if(!StringUtils.isEmpty(currentUser)){
            queryWrapper.eq(SRole::getCreateById,currentUser.getId());
        }
        //queryWrapper.setEntity(user);

        return this.page(page,queryWrapper);
    }


}
