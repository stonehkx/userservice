package com.csdn.userservice.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.csdn.userservice.model.SUser;

/**
 * @author StoneHkx
 * @ClassName SUserService
 * @Description TODO
 * @createDate 2020-01-05 11:50
 * @updatePerson
 * @updateDate
 */
public interface SUserService  extends IService<SUser> {
    SUser saveUser(SUser user) throws Exception;
    SUser updateUser(SUser user) throws Exception;
    IPage<SUser> selectUser(SUser user);
    SUser getUserByNamePassword(SUser user) throws Exception;
}
