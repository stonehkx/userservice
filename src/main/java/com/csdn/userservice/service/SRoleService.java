package com.csdn.userservice.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.csdn.userservice.model.SRole;
import com.csdn.userservice.model.SUser;

/**
 * @author StoneHkx
 * @ClassName SRoleService
 * @Description TODO
 * @createDate 2020-01-05 15:33
 * @updatePerson
 * @updateDate
 */
public interface SRoleService   extends IService<SRole> {
    SRole saveRole(SRole role,String token) throws Exception;

    SRole updateRole(SRole role) throws Exception;

    IPage<SRole> selectRole(SRole role,String token) throws Exception ;
}
