package com.csdn.userservice.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.csdn.userservice.model.SPermission;
import com.csdn.userservice.model.SRole;

/**
 * @author StoneHkx
 * @ClassName SPermissionService
 * @Description TODO
 * @createDate 2020-01-05 16:29
 * @updatePerson
 * @updateDate
 */
public interface SPermissionService extends IService<SPermission> {
    SPermission savePermission(SPermission permission) throws Exception;

    SPermission updatePermission(SPermission permission) throws Exception;

    IPage<SPermission> selectPermission(SPermission permission) throws Exception;
}
